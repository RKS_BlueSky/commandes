package com.Commandes.repository;

import com.Commandes.entity.Item;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@EnableJpaRepositories
public interface ItemRepository extends JpaRepository<Item,Long> {
}

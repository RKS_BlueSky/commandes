package com.Commandes.repository;

import com.Commandes.entity.ItemCommand;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ItemCommandRepository extends JpaRepository<ItemCommand, Long> {
}

# Commandes

# Cloning and launching a Spring Boot project with Corretto 18 in IntelliJ IDEA

## Prerequisites

- IntelliJ IDEA installed on your machine.
- Corretto 18 JDK installed on your machine.
- Maven installed on your machine and configured to use Corretto 18.

## Steps

1. Clone the Spring Boot project from GitHub using the following command in your terminal:  

    git clone https://github.com/Firadenda/FiradendaBack.git

2. Open IntelliJ IDEA and select "Open" from the "File" menu.

3. Navigate to the root directory of the cloned project and select it.

4. Wait for IntelliJ IDEA to finish loading the project.

5. Set up your JDK in IntelliJ IDEA:

- Open the "Project Structure" dialog by selecting "Project Structure" from the "File" menu.
- Click the "Project" tab on the left.
- Under "Project SDK", click the "New" button and select "JDK".
- Navigate to the directory where Corretto 18 is installed on your machine and select it.
- Click "OK" to close all windows.

6. Set up your Maven path in IntelliJ IDEA:

- Open the "Preferences" dialog by selecting "Preferences" from the "IntelliJ IDEA" menu on macOS, or "File" > "Settings" on Windows and Linux.
- Search for "Maven" in the search box.
- Click "Maven" under "Build, Execution, Deployment".
- In the "Maven home directory" field, enter the path to your Maven installation directory.
- Click "OK" to close all windows.

7. Sync Maven by clicking the "Reload All Maven Projects" button in the Maven toolbar.

8. Configure your database connection:

- Open the "application.properties" file in the "src/main/resources" directory.
- Set the following properties:

   ```
   spring.datasource.url=jdbc:mysql://localhost:3306/<your-database-schema>
   ```
   
   Replace `<your-database-schema>` with the name of the database schema that you want to use.
   
   You should set your database username and password as environment variables in your run configuration. To do this:
   
   - Click on the "Edit Configurations" button in the toolbar.
   - Select the "Spring Boot" configuration.
   - In the "Environment variables" section, add the following variables:
   
      ```
      DB_USERNAME=<your-database-username>
      DB_PASSWORD=<your-database-password>
      ```
      
      Replace `<your-database-username>` and `<your-database-password>` with your actual database username and password.
      
      Note: If you don't see an "Environment variables" input, click on the "Modify options" link and activate the "Environment variables" option.

9. Build the project by selecting "Build" from the "Build" menu.

10. Run the project by selecting the main class that contains the `main` method and clicking the green "Run" arrow button in the gutter next to it.

Congratulations, you have successfully cloned and launched a Spring Boot project with Corretto 18 in IntelliJ IDEA!


## CategoryController API
This API provides endpoints for managing categories.

Endpoints
The following endpoints are available:

Retrieve a category by name
Returns a category with the specified name.

Method: GET
Path: /category/getByName
Parameters:
name (query parameter, String): The name of the category to retrieve.

Retrieve all categories
Returns a list of all categories.

Method: GET
Path: /category
Parameters: None

Update a category
Updates a category with the specified ID.

Method: PUT
Path: /category/{id}
Parameters:
id (path variable, Long): The ID of the category to update.
categoryDetails (request body, Category): The updated category details.

Delete a category by ID
Deletes a category with the specified ID.

Method: DELETE
Path: /category/{id}
Parameters:
id (path variable, Long): The ID of the category to delete.

Delete a category by name
Deletes a category with the specified name.

Method: DELETE
Path: /category/deleteByName
Parameters:
name (query parameter, String): The name of the category to delete.

Create a category
Creates a new category.

Method: POST
Path: /category
Parameters:
category (request body, Category): The category object to create.


## CommandController API
This API provides endpoints for managing commands.

Endpoints
The following endpoints are available:

Get all commands
Returns a list of all commands.

Method: GET
Path: /command
Parameters: None

Get a command by ID
Returns a command with the specified ID.

Method: GET
Path: /command/{id}
Parameters:
id (path variable, Long): The ID of the command to retrieve.

Create a command
Creates a new command.

Method: POST
Path: /command
Parameters:
command (request body, Command): The command object to create.

Update a command
Updates a command with the specified ID.

Method: PUT
Path: /command/{id}
Parameters:
id (path variable, Long): The ID of the command to update.
command (request body, Command): The updated command object.

Delete a command
Deletes a command with the specified ID.

Method: DELETE
Path: /command/{id}
Parameters:
id (path variable, Long): The ID of the command to delete.

## CreditCardController API
This API provides endpoints for managing credit cards.

Endpoints
The following endpoints are available:

Get all credit cards
Returns a list of all credit cards.

Method: GET
Path: /creditcard
Parameters: None

Get a credit card by ID
Returns a credit card with the specified ID.

Method: GET
Path: /creditcard/{id}
Parameters:
id (path variable, Long): The ID of the credit card to retrieve.

Create a credit card
Creates a new credit card.

Method: POST
Path: /creditcard
Parameters:
creditCard (request body, CreditCard): The credit card object to create.

Update a credit card
Updates a credit card with the specified ID.

Method: PUT
Path: /creditcard/{id}
Parameters:
id (path variable, Long): The ID of the credit card to update.
creditCard (request body, CreditCard): The updated credit card object.

Delete a credit card
Deletes a credit card with the specified ID.

Method: DELETE
Path: /creditcard/{id}
Parameters:
id (path variable, Long): The ID of the credit card to delete.

## ItemController API
This API provides endpoints for managing items.

Endpoints
The following endpoints are available:

Get all items
Returns a list of all items.

Method: GET
Path: /items
Parameters: None

Get an item by ID
Returns an item with the specified ID.

Method: GET
Path: /items/{id}
Parameters:
id (path variable, Long): The ID of the item to retrieve.

Create an item
Creates a new item.

Method: POST
Path: /items
Parameters:
item (request body, Item): The item object to create.

Update item stock
Updates the stock of an item with the specified ID.

Method: PUT
Path: /items/{id}/updateStock
Parameters:
id (path variable, Long): The ID of the item to update.
newStock (query parameter, Integer): The new stock value.

Update an item
Updates an item with the specified ID.

Method: PUT
Path: /items/{id}
Parameters:
id (path variable, Long): The ID of the item to update.
item (request body, Item): The updated item object.

Delete an item by ID
Deletes an item with the specified ID.

Method: DELETE
Path: /items/{id}
Parameters:
id (path variable, Long): The ID of the item to delete.

Delete all items
Deletes all items.

Method: DELETE
Path: /items
Parameters: None